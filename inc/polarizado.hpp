#ifndef POLARIZADO_HPP
#define POLARIZADO_HPP
#include "imagem.hpp"
#include <iostream>

using namespace std;

class filtroPolarizado : public Imagem {
public:

  filtroPolarizado();
  ~filtroPolarizado(){};
  filtroPolarizado(int largura, int altura, int intensidade_pixel);
  void pixelImagem(ofstream &arquivoSaida);

};

#endif
