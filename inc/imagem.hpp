#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <iostream>
#include <fstream>
#include <string>
#include <cstdio>
#include <cstdlib>




using namespace std;

class Imagem {
private:

  string formato;
  int altura;
  int largura;
  int intensidade_pixel;

public:
  unsigned char **R;
  unsigned char **G;
  unsigned char **B;

  ofstream arquivoSaida;

  Imagem();
  ~Imagem();
  Imagem(string formato,  int altura,  int largura,  int intensidade_pixel);

  void setFormato(string formato);
  string getFormato(){return formato;};

   int getAltura(){return altura;};
  void setAltura( int altura);

   int getLargura(){return largura;};
  void setLargura( int largura);

   int getIntensidade_pixel(){return intensidade_pixel;};
  void setIntensidade_pixel( int intensidade_pixel);

  void dadosImagem(ofstream &arquivoSaida);
  virtual void pixelImagem(ofstream &arquivoSaida);


};



#endif
