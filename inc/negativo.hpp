#ifndef NEGATIVO_HPP
#define NEGATIVO_HPP
#include "imagem.hpp"
#include <iostream>

using namespace std;

class filtroNegativo : public Imagem {
public:

  filtroNegativo();
  ~filtroNegativo(){};
  filtroNegativo(int largura, int altura, int intensidade_pixel);
  void pixelImagem(ofstream &arquivoSaida);

};

#endif
