#ifndef PEB_HPP
#define PEB_HPP
#include "imagem.hpp"

using namespace std;

class PeB : public Imagem {
public:

  PeB();
  ~PeB(){};
  PeB(unsigned int largura, unsigned int altura, unsigned int intensidade_pixel);
  void pixelImagem(ofstream &arquivoSaida);

};

#endif
