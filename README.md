# EP1 - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

## Neste arquivo deve conter as instruções de execução e descrição do projeto.

Primeiramente será necessário a copia do repositório do mesmo projeto, contendo todas as pastas, inclusive
a pasta "copias" onde serão colocadas as imagens após a aplicação do filtro, feito isso entre na raiz do 
projeto pelo terminal, e execute os seguintes comandos, "make clean", "make", "make run". e o programa já irá funcionar.
Caso escolha a opção 5 do menu, não ultilize nomes iguais para todos os arquivos, isso fará o programa sobrescrevê-los,
restando assim somente o último filtro aplicado.

Obs: o filtro de média não foi implementado.

