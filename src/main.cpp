#include "imagem.hpp"
#include "PeB.hpp"
#include "negativo.hpp"
#include "polarizado.hpp"
#include <unistd.h>

int main(){


  int opcao;

  ofstream arquivoSaida;
  //Imagem * imagem = new Imagem();
  PeB *peb = new PeB();
  filtroNegativo *neg = new filtroNegativo();
  filtroPolarizado *pol = new filtroPolarizado();

  cout << "Escolha uma das opções abaixo: " << endl;

  cout << "#--------------------------------#" << endl;
  cout << "" << endl;

  cout << "(1) - Aplica Filtro Preto e Branco" << endl;
  cout << "(2) - Aplica Filtro Polarizado" << endl;
  cout << "(3) - Aplica Filtro de Média" << endl;
  cout << "(4) - Aplica Filtro Negativo" << endl;
  cout << "(5) - Aplica todos os Filtros" << endl;

  cout << "" << endl;
  cout << "#--------------------------------#" << endl;
  cin >> opcao;
  sleep(1);



  switch (opcao) {
    case 1:

    cout << "Aplicando FIltro Preto e Branco... " << endl;
    peb->dadosImagem(arquivoSaida);
    cout << "#--------------------------------#" << endl;
    break;

    case 2:

    cout << "Aplicando FIltro Polarizado..." << endl;
    pol->dadosImagem(arquivoSaida);
    cout << "#--------------------------------#" << endl;
    break;

    case 3:

    cout << "Aplicando FIltro de Média..." << endl;
    //filtro de media
    cout << "#---------------Não Implementado-----------------#" << endl;
    break;

    case 4:

    cout << "Aplicando FIltro Negativo... " << endl;
    neg->dadosImagem(arquivoSaida);
    cout << "#--------------------------------#" << endl;
    break;

    case 5:

    cout << "Aplicando FIltro Todos os Filtros..." << endl;
    peb->dadosImagem(arquivoSaida);
    neg->dadosImagem(arquivoSaida);
    pol->dadosImagem(arquivoSaida);
    cout << "#--------------------------------#" << endl;

    break;

    default: cout << "Erro! Opção inválida.   " << endl;
  }


  return 0;
}
