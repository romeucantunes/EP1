#include "polarizado.hpp"





filtroPolarizado::filtroPolarizado(int largura, int altura, int intensidade_pixel) {
  setAltura(altura);
  setLargura(largura);
  setIntensidade_pixel(intensidade_pixel);

}

filtroPolarizado::filtroPolarizado() {

    setAltura(0);
    setLargura(0);
    setIntensidade_pixel(0);

}



void filtroPolarizado::pixelImagem(ofstream &arquivoSaida) {
  int altura;
  int largura;
  int intensidade_pixel;
  int verm;
  int azul;
  int verd;
  int i,j;



  largura = getLargura();
  altura = getAltura();
  intensidade_pixel = getIntensidade_pixel();

  for (i = 0; i < altura; i++){
    for(j = 0; j < largura; j++){
      verm = R[i][j];
      if (verm > intensidade_pixel/2){
        R[i][j] = intensidade_pixel;
      }else {
        R[i][j] = 0;
      }

      verd = G[i][j];
      if (verd > intensidade_pixel/2){
        G[i][j] = intensidade_pixel;
      }else {
        G[i][j] = 0;
      }

      azul = B[i][j];
      if (azul > intensidade_pixel/2){
        B[i][j] = intensidade_pixel;
      }else {
        B[i][j] = 0;
      }

      arquivoSaida << (char)R[i][j];
      arquivoSaida << (char)G[i][j];
      arquivoSaida << (char)B[i][j];
    }

  }
}
