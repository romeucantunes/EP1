#include "negativo.hpp"
#include <cstdio>
#include <cstdlib>

using namespace std;

filtroNegativo::filtroNegativo() {

  setLargura(0);
  setAltura(0);
  setIntensidade_pixel(0);

}

filtroNegativo::filtroNegativo( int largura,  int altura,  int intensidade_pixel) {

    setAltura(altura);
    setLargura(largura);
    setIntensidade_pixel(intensidade_pixel);
}

void filtroNegativo::pixelImagem(ofstream &arquivoSaida) {
  int largura;
  int altura;
  int intensidade_pixel;

  largura = getLargura();
  altura = getAltura();
  intensidade_pixel = getIntensidade_pixel();

  int i;
  int k;

  for(i = 0; i < altura; i++) {
    for (k = 0; k < largura; k++) {

      R[i][k] = intensidade_pixel - R[i][k];
      arquivoSaida << R[i][k];

      G[i][k] = intensidade_pixel - G[i][k];
      arquivoSaida << G[i][k];

      B[i][k] = intensidade_pixel - B[i][k];
      arquivoSaida << B[i][k];


    }
  }
}
