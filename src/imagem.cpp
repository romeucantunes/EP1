#include "imagem.hpp"
#include <fstream>
#include <iostream>
#include <string>
#include <unistd.h>

using namespace std;

Imagem::Imagem(string formato, int altura, int largura, int intensidade_pixel){
  this->formato = formato;
  this->altura = altura;
  this->largura = largura;
  this->intensidade_pixel = intensidade_pixel;
}

Imagem::~Imagem(){

}

Imagem::Imagem(){
  formato = "";
  largura = 0;
  altura = 0;
  intensidade_pixel = 255;

  R = new unsigned char*[altura];
  for(int i = 0; i < altura; i++) {
    R[i] = new unsigned char[largura];
  }

  G = new unsigned char*[altura];
  for(int i = 0; i < altura; i++) {
    G[i] = new unsigned char[largura];
  }

  B = new unsigned char*[altura];
  for(int i = 0; i < altura; i++) {
    B[i] = new unsigned char[largura];
  }
}



void Imagem::setLargura(int largura){
  this->largura = largura;
}

void Imagem::setAltura(int altura){
  this->altura = altura;
}

void Imagem::setIntensidade_pixel(int intensidade_pixel){
  this->intensidade_pixel = intensidade_pixel;
}

void Imagem::setFormato(string formato){
  this->formato = formato;
}


void Imagem::dadosImagem(ofstream &arquivoSaida){

    ifstream arquivo;
    string nomeArquivo;
    system("clear");

    cout << "#--------------------------------#" << endl;
    cout << "Insira o nome do Arquivo desejado, sem a extensão" << endl;

    cin >> nomeArquivo;
    nomeArquivo = "./doc/" + nomeArquivo + ".ppm";

    arquivo.open(nomeArquivo.c_str(), ios::binary);


if (arquivo.is_open() && arquivo.good()) {

      cout << "Arquivo Aberto Com Sucesso!\n" << endl;
      cout << "#--------------------------------#" << endl;
      sleep(1);
      system("clear");


      int altura;
      int largura;
      int intensidade_pixel;
      string formato;
      streampos before;
      char c;
      char comentario[1024];

      arquivo.seekg(0);
      arquivo >> formato;

      do {
        before = arquivo.tellg();
        arquivo >> c;
        if (c == '#'){
          arquivo.getline(comentario, 1024);
        }

        else {
          arquivo.seekg(before);
        }
      } while (c == '#');

      arquivo >> largura >> altura;
      arquivo >> intensidade_pixel;


      setFormato(formato);
      setLargura(largura);
      setAltura(altura);
      setIntensidade_pixel(intensidade_pixel);


      arquivo.seekg(1,ios_base::cur);

      cout << "#--------------------------------#" << endl;


      string nome1;
      cout << "insira Nome do Arquivo para Copiar" << endl;
      cin >> nome1;
      nome1 = "./copias/" + nome1 + ".ppm";

      arquivoSaida.open(nome1.c_str());

      if(arquivoSaida.is_open() && arquivoSaida.good()) {

          arquivoSaida << formato << endl;
          arquivoSaida << largura << " " << altura << endl;
          arquivoSaida << intensidade_pixel << endl;

          int j,k;
          char r;
          char g;
          char b;

          R = new unsigned char*[altura];
        	for(int i = 0; i < altura; i++){
        	R[i] = new unsigned char[largura];
          }

          G = new unsigned char*[altura];
        	for(int i = 0; i < altura; i++){
        	G[i] = new unsigned char[largura];
          }

          B = new unsigned char*[altura];
        	for(int i = 0; i < altura; i++){
        	B[i] = new unsigned char[largura];
          }

            for(j = 0; j < altura; j++) {
              for(k = 0; k < largura; k++) {
              arquivo.get(r);
              arquivo.get(g);
              arquivo.get(b);
              R[j][k] = r;
              G[j][k] = g;
              B[j][k] = b;
              }
          }

        } else {
          cout << "Não Foi Possivel Abrir o Arquivo, Copia! \n\n\n\n" << endl;
        }
      } else {
        cout << "Não Foi Possivel Abrir o Arquivo, Original! \n\n\n\n" << endl;
      }

        pixelImagem(arquivoSaida);

        //copia do arquivo original

        arquivo.seekg(0);
        ofstream arquivo_copia;

        arquivo_copia.open("./copias/copia_do_original.ppm");
        arquivo_copia << arquivo.rdbuf();

        // -- //


        arquivo.close();
        arquivoSaida.close();

}

  void Imagem::pixelImagem(ofstream &arquivoSaida){

       int **Vermelho;
       int **Verde;
       int **Azul;

      Vermelho = new int*[altura];
      for(int i = 0; i < altura; i++) {
      Vermelho[i] = new int[largura];
      }

      Verde = new int*[altura];
      for(int i = 0; i < altura; i++) {
      Verde[i] = new int[largura];
      }

      Azul = new int*[altura];
      for(int i = 0; i < altura; i++) {
      Azul[i] = new int[largura];
      }

      int i;
      int j;

      for(i = 0; i < altura; i++) {
          for( j = 0; j < largura; j++) {
             Vermelho[i][j] = (unsigned int)R[i][j];
             arquivoSaida << (char)Vermelho[i][j];

             Verde[i][j] = (unsigned int)G[i][j];
             arquivoSaida << (char)Verde[i][j];

             Azul[i][j] = (unsigned int)B[i][j];
             arquivoSaida << (char)Azul[i][j];
             }
         }
    }
