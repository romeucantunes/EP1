#include "PeB.hpp"
#include <fstream>
#include <iostream>
#include <string>


using namespace std;


PeB::PeB() {

  setAltura(0);
  setLargura(0);
  setIntensidade_pixel(0);

}

PeB::PeB(unsigned int largura, unsigned int altura, unsigned int intensidade_pixel) {

  setLargura(largura);
  setAltura(altura);
  setIntensidade_pixel(intensidade_pixel);

}

void PeB::pixelImagem(ofstream &arquivoSaida) {
  unsigned int largura;
  unsigned int altura;
  unsigned int intensidade_pixel;

  largura = getLargura();
  altura = getAltura();
  intensidade_pixel = getIntensidade_pixel();

  unsigned int k,r;

  unsigned int cinza;

  for (k = 0; k < altura; k++) {
    for(r = 0; r < largura; r++){
      cinza = (0.299*R[k][r]) +  (0.587*G[k][r]) + (0.144*B[k][r]);

     if (cinza > intensidade_pixel) {
        R[k][r] = intensidade_pixel;
        G[k][r] = intensidade_pixel;
        B[k][r] = intensidade_pixel;

        arquivoSaida << R[k][r];
        arquivoSaida << G[k][r];
        arquivoSaida << B[k][r];
     } else {

       R[k][r] = (char) cinza;
       G[k][r] = (char) cinza;
       B[k][r] = (char) cinza;
       arquivoSaida << R[k][r];
       arquivoSaida << G[k][r];
       arquivoSaida << B[k][r];

     }
    }
  }
}
